<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Pokemon;
use App\User;
use Auth;
use App\Http\Controllers\Controller;

class PokemonsController extends Controller
{

  protected $pokemon, $user;

  public function __construct(Pokemon $pokemon, User $user)
  {
      $this->pokemon = $pokemon;
      $this->user = $user;
  }

  public function index()
  {
      return $this->pokemon->with('user')->orderBy('id','ASC')->get()->toJson();
  }

  public function update(Request $request)
  {
    $update_image = $this->user->find(Auth::user()->id);
    $update_image->pokemon_id = $request->pokemon_id;
    $update_image->save();
    return response(['message' => 'El avatar de su perfil ha sido actualizado'], 200);
  }
}
