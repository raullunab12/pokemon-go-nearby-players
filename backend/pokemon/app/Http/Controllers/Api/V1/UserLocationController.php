<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Auth;
use App\UserLocation;
use App\User;
use App\Http\Requests;
use App\Lib\RealTimeLocation;
use App\Http\Controllers\Controller;

class UserLocationController extends Controller
{

  protected $user_location;

  public function __construct(UserLocation $user_location, User $user)
  {
      $this->user_location = $user_location;
      $this->user = $user;
  }

  public function store(Request $request)
  {
    $store_user_location = $this->user_location;
    $store_user_location->user_id = Auth::user()->id;
    $store_user_location->latitude = $request->latitude;
    $store_user_location->longitude = $request->longitude;
    $store_user_location->save();

    $user = $this->user->find($store_user_location->user_id);
    $user_object = ["email" => $user->email, "name" => $user->name, "latitude" => $user->latitude, "longitude" => $user->longitude, "team" => $user->team, "avatar" => $user->pokemon->url];
    RealTimeLocation::send_location($user->team, json_encode($user_object));
    return response(['message' => 'Su ubicación ha sido actualizada'], 200);
  }
}
