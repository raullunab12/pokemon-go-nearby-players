<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'web'], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::auth();
    Route::get('/home', 'HomeController@index');


    Route::group(['middleware' => 'auth'], function ($api) {

    });
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

    $api->post('auth/login', 'App\Http\Controllers\Api\V1\AuthController@login');
    $api->post('auth/signup', 'App\Http\Controllers\Api\V1\AuthController@signup');
    $api->post('auth/recovery', 'App\Http\Controllers\Api\V1\AuthController@recovery');
    $api->post('auth/reset', 'App\Http\Controllers\Api\V1\AuthController@reset');

    $api->group(['middleware' => 'api.auth'], function ($api) {
      //$api->get('users/{id}', 'App\Api\Controllers\UserController@show');
      $api->post('user_location', 'App\Http\Controllers\Api\V1\UserLocationController@store');
      $api->get('pokemons', 'App\Http\Controllers\Api\V1\PokemonsController@index');
      $api->put('update_pokemon', 'App\Http\Controllers\Api\V1\PokemonsController@update');
    });

});
