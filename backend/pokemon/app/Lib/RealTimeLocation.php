<?php

namespace App\Lib;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RealTimeLocation
{
    public static function send_location($team, $message)
    {
      $connection = new AMQPStreamConnection('server', 5672, 'user', 'password', 'vhost');
      $channel = $connection->channel();
      $channel->exchange_declare($team, 'fanout', false, false, false);
      $msg = new AMQPMessage($message);
      $channel->basic_publish($msg, $team);
      $channel->close();
      $connection->close();
    }
}
