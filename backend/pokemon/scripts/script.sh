#!/usr/bin/env bash

echo "publish site"
sudo cp -rf /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/app /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/config /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/bootstrap /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/database /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/public /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/resources /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/tests /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/vendor /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/storage /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/composer.json /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/phpunit.xml /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/server.php /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/.env /var/www/html
sudo cp -rf  /home/ec2-user/pokemon-go-nearby-players/backend/pokemon/artisan /var/www/html
sudo chmod -R 777 /var/www/html/
echo "publish updated"
