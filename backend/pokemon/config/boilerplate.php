<?php



return [
    'signup_fields' => ['name', 'email', 'password', 'latitude', 'longitude', 'team'],
    'signup_token_release' => true,
    'signup_fields_rules' => [
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6',
        'team' => 'required|in:red,blue,yellow'
    ],
];
