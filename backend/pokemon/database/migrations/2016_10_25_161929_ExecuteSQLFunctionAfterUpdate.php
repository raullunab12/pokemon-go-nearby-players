<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExecuteSQLFunctionAfterUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users', function(Blueprint $table){
        $sql = "CREATE OR REPLACE FUNCTION function_copy() RETURNS TRIGGER AS \$BODY$ BEGIN UPDATE public.users SET latitude=new.latitude, longitude=new.longitude WHERE id = new.user_id; RETURN new; END; \$BODY$ language plpgsql; CREATE TRIGGER trig_copy AFTER INSERT ON user_locations FOR EACH ROW EXECUTE PROCEDURE function_copy();";
        DB::connection()->getPdo()->exec($sql);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
