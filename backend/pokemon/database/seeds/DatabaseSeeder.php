<?php

use Illuminate\Database\Seeder;
use App\Pokemon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //$this->call(PokemonsTableSeeder::class);
         $pokemons = array(
           ['name'=>'Pikachu','url'=>'/images/Pikachu.png'],
           ['name'=>'Bulbasaur','url'=>'/images/Bulbasaur.png'],
           ['name'=>'Charmander','url'=>'/images/Charmander.png'],
           ['name'=>'Squirtle','url'=>'/images/Squirtle.png'],
           ['name'=>'Chikorita','url'=>'/images/Chikorita.png'],
           ['name'=>'Cyndaquil','url'=>'/images/Cyndaquil.png'],
           ['name'=>'Totodile','url'=>'/images/Totodile.png'],
           ['name'=>'Treecko','url'=>'/images/Treecko.png'],
           ['name'=>'Torchic','url'=>'/images/Torchic.png'],
           ['name'=>'Mudkip','url'=>'/images/Mudkip.png'],
           ['name'=>'Turtwig','url'=>'/images/Turtwig.png'],
           ['name'=>'Chimchar','url'=>'/images/Chimchar.png'],
           ['name'=>'Piplup','url'=>'/images/Piplup.png']
        );

        foreach ($pokemons as $pokemon)
        {
            Pokemon::create($pokemon);
        }


    }
}
