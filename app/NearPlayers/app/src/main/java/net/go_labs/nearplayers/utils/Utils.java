package net.go_labs.nearplayers.utils;

import android.content.Context;
import android.content.SharedPreferences;

import net.go_labs.nearplayers.dtos.APIError;
import net.go_labs.nearplayers.services.IAuthService;
import net.go_labs.nearplayers.services.RequestInterceptors;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by raulluna on 25/10/16.
 */

public class Utils {

    public static String getToken(Context pContext){
        SharedPreferences sharedPreferences = pContext.getSharedPreferences(Constants.SHARED_PREFERENCES_NAME, pContext.MODE_PRIVATE);
        return "bearer " + sharedPreferences.getString(Constants.TOKEN_VALUE, "");
    }

    public static APIError parseError(Response<?> response) {
        Converter<ResponseBody, APIError> converter =
                RequestInterceptors.retrofit()
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }
}
