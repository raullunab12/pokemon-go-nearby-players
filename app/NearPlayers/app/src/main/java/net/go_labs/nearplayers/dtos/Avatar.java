package net.go_labs.nearplayers.dtos;

/**
 * Created by raulluna on 26/10/16.
 */

public class Avatar {
    int id;
    String url;
    User user;

    public Avatar(int id, String url, User user) {
        this.id = id;
        this.url = url;
        this.user = user;
    }

    public Avatar() {
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public User getUser() {
        return user;
    }
}
