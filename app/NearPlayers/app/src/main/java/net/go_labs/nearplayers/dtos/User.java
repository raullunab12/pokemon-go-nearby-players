package net.go_labs.nearplayers.dtos;

import org.parceler.Parcel;

/**
 * Created by carlos on 9/29/16.
 */

@Parcel
public class User {
    int id;
    String name;
    String email;
    String password;
    String team;
    String avatar;
    double latitude;
    double longitude;

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(String name, String email, String password, String team, double latitude, double longitude) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.team = team;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getTeam() {
        return team;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getName() {
        return name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
