package net.go_labs.nearplayers.dtos;

import java.util.ArrayList;

/**
 * Created by raulluna on 26/10/16.
 */

public class APIError {
    private ArrayList<String> errors;
    private String message;

    public APIError() {
        errors = new ArrayList<>();
        message = "";
    }

    public ArrayList<String> errors() {
        return errors;
    }

    public String message() {
        return message;
    }
}
