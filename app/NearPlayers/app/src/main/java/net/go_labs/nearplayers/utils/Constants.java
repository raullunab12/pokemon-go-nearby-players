package net.go_labs.nearplayers.utils;

/**
 * Created by carlos on 9/30/16.
 */

public class Constants {
    public static final String BASE_URL                 = "";
    public static final String TOKEN                    = "token";
    public static final String SHARED_PREFERENCES_NAME  = "pokemon_preferences";
    public static final String EXCHANGE_TYPE            = "fanout";
    public static final String CLOUDAMQP_URI            = "";
    public static final String TOKEN_VALUE              = "token";
}
