package net.go_labs.nearplayers.dtos;


import org.parceler.Parcel;

/**
 * Created by carlos on 9/30/16.
 */

@Parcel
public class AuthUser {
    String token;
    User user;
    public String getToken() {
        return token;
    }

    public AuthUser() {

    }

    public AuthUser(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
