package net.go_labs.nearplayers.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;


import com.squareup.picasso.Picasso;

import net.go_labs.nearplayers.R;
import net.go_labs.nearplayers.dtos.APIError;
import net.go_labs.nearplayers.dtos.Avatar;
import net.go_labs.nearplayers.dtos.Pokemon;
import net.go_labs.nearplayers.services.IAuthService;
import net.go_labs.nearplayers.services.RequestInterceptors;
import net.go_labs.nearplayers.utils.Constants;
import net.go_labs.nearplayers.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by raulluna on 26/10/16.
 */

public class AvatarAdapters extends RecyclerView.Adapter<AvatarAdapters.AvatarsViewHolder>{

    private List<Avatar> avatars;
    private ProgressDialog vDialog;
    private Context context;

    public class AvatarsViewHolder extends RecyclerView.ViewHolder {
        ImageView avatarImage;
        Avatar item;
        AvatarsViewHolder(View itemView) {
            super(itemView);
            avatarImage = (ImageView) itemView.findViewById(R.id.avatar_row_image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chooseAvatar(v, item);
                }
            });
            avatarImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chooseAvatar(v, item);
                }
            });
        }
    }

    private void chooseAvatar(View v, final Avatar item){
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Choosing avatar")
                .setMessage("Are you sure you want to choose this avatar?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendRequest(item);
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private void sendRequest(Avatar item){
        vDialog.setMessage(context.getResources().getString(R.string.loading));
        vDialog.show();
        RequestInterceptors connect = new RequestInterceptors();
        IAuthService service = connect.CEServiceWithToken(context);
        Call<APIError> call = service.updateAvatar(new Pokemon(item.getId()));
        call.enqueue(new Callback<APIError>() {
            @Override
            public void onResponse(Call<APIError> call, retrofit2.Response<APIError> response) {
                vDialog.dismiss();
                if (response.isSuccessful()) {
                    Toast.makeText(context, response.body().message(), Toast.LENGTH_LONG).show();
                    ((Activity)context).finish();
                } else {
                    APIError error = Utils.parseError(response);
                    Toast.makeText(context, error.message(), Toast.LENGTH_LONG).show();
                    ((Activity)context).finish();
                }
            }
            @Override
            public void onFailure(Call<APIError> call, Throwable t) {
                vDialog.dismiss();
                Toast.makeText(context, context.getResources().getString(R.string.default_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    public AvatarAdapters(Context context, ProgressDialog vDialog){
        this.avatars = new ArrayList<>();
        this.context = context;
        this.vDialog = vDialog;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public AvatarsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_recycler_avatar, viewGroup, false);
        return new AvatarsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AvatarsViewHolder avatarViewHolder, int i) {
        avatarViewHolder.item  = avatars.get(i);
        Picasso.with(context)
                .load(Constants.BASE_URL + avatars.get(i).getUrl())
                .error(R.color.colorPrimary)
                .into(avatarViewHolder.avatarImage);
    }

    @Override
    public int getItemCount() {
        return avatars.size();
    }

    public void addAll(List<Avatar> avatars) {
        for (Avatar item : avatars) {
            add(item);
        }
    }

    private void add(Avatar item) {
        avatars.add(item);
        notifyItemInserted(avatars.size()-1);
    }
}