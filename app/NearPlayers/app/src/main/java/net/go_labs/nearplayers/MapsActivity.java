package net.go_labs.nearplayers;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.squareup.picasso.Picasso;

import net.go_labs.nearplayers.dtos.APIError;
import net.go_labs.nearplayers.dtos.AuthUser;
import net.go_labs.nearplayers.dtos.User;
import net.go_labs.nearplayers.services.IAuthService;
import net.go_labs.nearplayers.services.RequestInterceptors;
import net.go_labs.nearplayers.utils.Constants;
import net.go_labs.nearplayers.utils.PicassoMarker;
import net.go_labs.nearplayers.utils.Utils;

import org.parceler.Parcels;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, ConnectionCallbacks, OnConnectionFailedListener, LocationListener, ActivityCompat.OnRequestPermissionsResultCallback {

    private GoogleMap mMap;
    @BindView(R.id.btn_manage_avatar)
    FloatingActionButton floatingActionButton;
    @BindView(R.id.maps_activity_root)
    RelativeLayout mapsActivityRoot;
    Thread subscribeThread;
    Map<String, PicassoMarker> markers;
    private static final int REQUEST_ACCESS_FINE_LOCATION = 1;

    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        mayRequestLocation();
        buildGoogleApiClient();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setSmallestDisplacement(3);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        if (!(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStop() {
        mCurrentLocation = null;
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        if (mCurrentLocation == null) {
            if (!(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                // TODO: Consider calling
                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                /// updateUI();
                startLocationUpdates();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        postLocation(location);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Toast.makeText(this, result.getErrorMessage(),
                Toast.LENGTH_SHORT).show();
    }

    private void postLocation(Location location){
        RequestInterceptors connect = new RequestInterceptors();
        IAuthService service = connect.CEServiceWithToken(MapsActivity.this);
        Call<APIError> call = service.updateLocation(new User("", "", "", "", location.getLatitude(), location.getLongitude()));
        call.enqueue(new Callback<APIError>() {
            @Override
            public void onResponse(Call<APIError> call, retrofit2.Response<APIError> response) {
                if (!response.isSuccessful()) {
                    APIError error = Utils.parseError(response);
                    Toast.makeText(MapsActivity.this, error.message(), Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<APIError> call, Throwable t) {
                Toast.makeText(MapsActivity.this, getResources().getString(R.string.default_error), Toast.LENGTH_LONG).show();
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (!(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            mMap.setMyLocationEnabled(true);
        }
        markers = new HashMap<String, PicassoMarker>();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(10.328666, -84.429469)));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(18));
        this.subscribe();
    }

    private void onMessageReceived(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Gson gson = new Gson();
                User userUpdate = gson.fromJson(text, User.class);
                try {
                    Picasso.with(MapsActivity.this).load(Constants.BASE_URL + userUpdate.getAvatar()).into(findOrCreateMarker(userUpdate.getName(), userUpdate));
                } catch (Exception e) {
                    e.getMessage();
                    e.printStackTrace();
                }
            }
        });
    }

    private PicassoMarker findOrCreateMarker(String key, User userUpdate){
        PicassoMarker marker;
        if (markers.containsKey(key)) {
            marker = markers.get(key);
            marker.getmMarker().setPosition(new LatLng(userUpdate.getLatitude(), userUpdate.getLongitude()));
        } else {
            marker = new PicassoMarker(mMap.addMarker(new MarkerOptions().position(new LatLng(userUpdate.getLatitude(), userUpdate.getLongitude())).title(userUpdate.getName())));
            markers.put(key, marker);
        }
        return marker;
    }

    private void subscribe(){
        subscribeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                AuthUser user = (AuthUser) Parcels.unwrap(getIntent().getParcelableExtra("user"));
                try {
                    Channel channel = AmqpProvider.getInstance().getReadChannel();
                    channel.exchangeDeclare(user.getUser().getTeam(), Constants.EXCHANGE_TYPE);
                    String queueName = channel.queueDeclare().getQueue();
                    channel.queueBind(queueName, user.getUser().getTeam(), "");
                    Consumer consumer = new DefaultConsumer(channel) {
                        @Override
                        public void handleDelivery(String consumerTag, Envelope envelope,
                                                   AMQP.BasicProperties properties, byte[] body) throws IOException {
                            onMessageReceived(new String(body, "UTF-8"));
                        }
                    };
                    channel.basicConsume(queueName, true, consumer);
                } catch (Exception e) {
                    e.getMessage();
                    e.printStackTrace();
                }
            }
        });
        subscribeThread.start();
    }

    @OnClick(R.id.btn_manage_avatar)
    public void attemptAvatar() {
        Intent intent = new Intent(MapsActivity.this, ChooseAvatar.class);
        startActivity(intent);
    }

    private boolean mayRequestLocation() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
            MapsActivity.this.requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                    REQUEST_ACCESS_FINE_LOCATION);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_ACCESS_FINE_LOCATION) {
            /*if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //populateAutoComplete();
            }*/
        }
    }
}
