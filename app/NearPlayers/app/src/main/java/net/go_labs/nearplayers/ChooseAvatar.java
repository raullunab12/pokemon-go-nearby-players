package net.go_labs.nearplayers;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import net.go_labs.nearplayers.adapters.AvatarAdapters;
import net.go_labs.nearplayers.dtos.APIError;
import net.go_labs.nearplayers.dtos.AuthUser;
import net.go_labs.nearplayers.dtos.Avatar;
import net.go_labs.nearplayers.dtos.User;
import net.go_labs.nearplayers.services.IAuthService;
import net.go_labs.nearplayers.services.RequestInterceptors;
import net.go_labs.nearplayers.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ChooseAvatar extends AppCompatActivity {

    private ProgressDialog vDialog;
    private AvatarAdapters adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_avatar);

        vDialog = new ProgressDialog(ChooseAvatar.this);
        vDialog.setCancelable(false);
        vDialog.setCanceledOnTouchOutside(false);

        RecyclerView recyclerView = (RecyclerView) this.findViewById(R.id.activity_recycler_list_avatars);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ChooseAvatar.this);
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new AvatarAdapters(ChooseAvatar.this, vDialog);
        recyclerView.setAdapter(adapter);
        getData();
    }

    private void getData(){
        vDialog.setMessage(getResources().getString(R.string.loading));
        vDialog.show();
        RequestInterceptors connect = new RequestInterceptors();
        IAuthService service = connect.CEServiceWithToken(ChooseAvatar.this);
        Call<List<Avatar>> call = service.avatars();
        call.enqueue(new Callback<List<Avatar>>() {
            @Override
            public void onResponse(Call<List<Avatar>> call, retrofit2.Response<List<Avatar>> response) {
                vDialog.dismiss();
                if (response.isSuccessful()) {
                    adapter.addAll(response.body());
                    if (response.body().size() > 0)
                        hideNoNewsMsg();
                } else {
                    if (response.raw().code() == 401) {
                        Toast.makeText(ChooseAvatar.this, getResources().getString(R.string.unauthorized_error), Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                    APIError error = Utils.parseError(response);
                    Toast.makeText(ChooseAvatar.this, error.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Avatar>> call, Throwable t) {
                vDialog.dismiss();
                Toast.makeText(ChooseAvatar.this, getResources().getString(R.string.default_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void hideNoNewsMsg(){
        TextView textInfo = (TextView) this.findViewById(R.id.no_avatars_available);
        textInfo.setVisibility(View.GONE);
    }
}
