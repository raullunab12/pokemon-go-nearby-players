package net.go_labs.nearplayers;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import net.go_labs.nearplayers.dtos.APIError;
import net.go_labs.nearplayers.dtos.AuthUser;
import net.go_labs.nearplayers.dtos.Avatar;
import net.go_labs.nearplayers.dtos.User;
import net.go_labs.nearplayers.services.IAuthService;
import net.go_labs.nearplayers.services.RequestInterceptors;
import net.go_labs.nearplayers.utils.Utils;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

import android.text.TextUtils;

public class SignInActivity extends AppCompatActivity {
    @BindView(R.id.register_username)
    EditText register_username;
    @BindView(R.id.register_email)
    EditText register_email;
    @BindView(R.id.register_team)
    Spinner register_team;
    @BindView(R.id.register_password)
    EditText register_password;
    String currentTeam = "";
    private ProgressDialog vDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        setSpinner();
        vDialog = new ProgressDialog(this);
        vDialog.setCancelable(false);
        vDialog.setCanceledOnTouchOutside(false);
    }

    private void setSpinner(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.team_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        register_team.setAdapter(adapter);
        register_team.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                // your code here
                if(position > 0)
                    currentTeam = (String) parentView.getItemAtPosition(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    @OnClick(R.id.register_submit)
    void submit() {
        String user = register_username.getText().toString();
        String email = register_email.getText().toString();
        String password = register_password.getText().toString();
        if(Objects.equals(currentTeam, "")){
            Toast.makeText(SignInActivity.this, getResources().getString(R.string.select_team), Toast.LENGTH_SHORT).show();
            return;
        }
        if (user.isEmpty() || email.isEmpty() || password.isEmpty()) {
            Toast.makeText(SignInActivity.this, getResources().getString(R.string.login_field_error), Toast.LENGTH_SHORT).show();
            return;
        }
        if(!isValidEmail(email)){
            Toast.makeText(SignInActivity.this, getResources().getString(R.string.email_invalid), Toast.LENGTH_SHORT).show();
            return;
        }
        signInRequest(user, email, password, currentTeam);
    }

    private void signInRequest(String pUser, String pEmail, String pPassword, String pTeam) {
        vDialog.setMessage(getResources().getString(R.string.loading));
        vDialog.show();
        RequestInterceptors connect = new RequestInterceptors();
        IAuthService service = connect.CEServiceWithoutToken();
        Call<AuthUser> call = service.register(new User(pUser, pEmail, pPassword, pTeam.toLowerCase(), 00, 00));
        call.enqueue(new Callback<AuthUser>() {
            @Override
            public void onResponse(Call<AuthUser> call, retrofit2.Response<AuthUser> response) {
                vDialog.dismiss();
                if (response.isSuccessful()) {
                    Toast.makeText(SignInActivity.this, "Registro completado satisfactoriamente.", Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    APIError error = Utils.parseError(response);
                    Toast.makeText(SignInActivity.this, TextUtils.join("\n", error.errors()), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<AuthUser> call, Throwable t) {
                vDialog.dismiss();
                Toast.makeText(SignInActivity.this, getResources().getString(R.string.default_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}
