package net.go_labs.nearplayers.services;

import net.go_labs.nearplayers.dtos.APIError;
import net.go_labs.nearplayers.dtos.AuthUser;
import net.go_labs.nearplayers.dtos.Avatar;
import net.go_labs.nearplayers.dtos.Pokemon;
import net.go_labs.nearplayers.dtos.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by carlos on 9/30/16.
 */

public interface IAuthService {
    @POST("/api/auth/login")
    Call<AuthUser> login(@Body User user);

    @POST("/api/auth/signup")
    Call<AuthUser> register(@Body User user);

    @GET("/api/pokemons")
    Call<List<Avatar>> avatars();

    @PUT("/api/update_pokemon")
    Call<APIError> updateAvatar(@Body Pokemon pokemon);

    @POST("/api/user_location")
    Call<APIError> updateLocation(@Body User user);
}
