package net.go_labs.nearplayers.services;

import android.content.Context;

import net.go_labs.nearplayers.utils.Constants;
import net.go_labs.nearplayers.utils.Utils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by carlos on 9/30/16.
 */

public class RequestInterceptors {

    public RequestInterceptors() {
    }

    public static Retrofit retrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public IAuthService CEServiceWithoutToken() {
        Retrofit retrofit =
                new Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .client(requestInterceptor())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
        return retrofit.create(IAuthService.class);
    }

    public IAuthService CEServiceWithToken(Context context){
        Retrofit retrofit =
                new Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .client(requestInterceptor())
                        .client(requestInterceptorToken(context))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
        return retrofit.create(IAuthService.class);
    }

    private OkHttpClient requestInterceptor() {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json").build();
                return chain.proceed(request);
            }
        });
        return builder.build();
    }

    private OkHttpClient requestInterceptorToken(final Context context) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Authorization", Utils.getToken(context)).build();
                return chain.proceed(request);
            }
        });
        return builder.build();
    }
}
